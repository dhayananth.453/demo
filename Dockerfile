#Base docker image
FROM openjdk:17
LABEL maintainer="Springboot"
ADD build/libs/springboot-0.0.1-SNAPSHOT.jar springboot.jar
ENTRYPOINT ["java", "-jar", "springboot.jar"]
